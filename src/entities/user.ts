import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import UserRole from "../types/userRole";
import { Family } from "./family";
import Wish from "./wish";

@Entity()
@ObjectType()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  @Field()
  id!: string;

  @Column({ length: 50, unique: true, nullable: false })
  @Field()
  username!: string;

  @Column({
    length: 255,
    unique: true,
    nullable: false,
  })
  @Field()
  email!: string;

  @Column({ type: "enum", enum: UserRole, default: UserRole.User })
  @Field()
  role!: UserRole;

  @Field((_) => [Family])
  @ManyToMany(() => Family, (family) => family.admins)
  adminOfFamilies!: Family[];

  @Field((_) => [Family])
  @ManyToMany(() => Family, (family) => family.members)
  families!: Family[];

  @OneToMany(() => Wish, (gift) => gift.user)
  @Field((_) => [Wish])
  wishes!: Wish[];

  @OneToMany(() => Wish, (gift) => gift.user)
  @Field((_) => [Wish])
  gifts!: Wish[];

  @Column({ nullable: false })
  password!: string;
}
