import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import Wish from "./wish";
import { User } from "./user";

@Entity()
@ObjectType()
export class Family extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  @Field()
  id!: string;

  @Column({ length: 75, nullable: false })
  @Field()
  name!: string;

  @ManyToMany(() => User)
  @JoinTable()
  @Field((_) => [User])
  admins!: User[];

  @ManyToMany(() => User)
  @JoinTable()
  @Field((_) => [User])
  members!: User[];

  @ManyToMany(() => Wish, (gift) => gift.families)
  @JoinTable()
  @Field((_) => [Wish])
  wishes!: Wish[];
}
