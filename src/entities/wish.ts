import { Field, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import GiftState from "../types/giftState";
import { Family } from "./family";
import { User } from "./user";

@Entity()
@ObjectType()
export default class Wish extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  @Field()
  id!: string;

  @Column({ length: 100, nullable: false })
  @Field()
  name!: string;

  @Column({ length: 300, nullable: true })
  @Field()
  link!: string;

  @Column({ type: "text", nullable: false })
  @Field()
  description!: string;

  @Column({ type: "enum", enum: GiftState, default: GiftState.New })
  @Field()
  state!: GiftState;

  @ManyToOne(() => User, (user) => user.wishes)
  @Field((_) => User)
  user!: User;

  @ManyToOne(() => User, (user) => user.gifts)
  @Field((_) => User, { nullable: true })
  giver?: User | null;

  @ManyToMany(() => Family, (family) => family.wishes)
  @Field((_) => [Family])
  families!: Family[];
}
