enum UserRole {
  User = "USER",
  Admin = "ADMIN",
}
export default UserRole;
