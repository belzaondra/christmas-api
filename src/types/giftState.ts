enum GiftState {
  New = "NEW",
  Pending = "PENDING",
  Finished = "FINISHED",
}
export default GiftState;
