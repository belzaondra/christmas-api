import {
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginLandingPageDisabled,
} from "apollo-server-core";
import { ApolloServer } from "apollo-server-express";
import connectRedis from "connect-redis";
import cors from "cors";
import "dotenv-safe/config";
import express from "express";
import session from "express-session";
import Redis from "ioredis";
import path from "path";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import { createConnection } from "typeorm";
import { COOKIE_NAME, __prod__ } from "./consants";
import { Family } from "./entities/family";
import Wish from "./entities/wish";
import { User } from "./entities/user";
import FamilyResolver from "./resolvers/families/familiesResolver";
import WishResolver from "./resolvers/wishes/wishResolver";
import UserResolver from "./resolvers/users/usersResolver";
const port = process.env.PORT;

const main = async () => {
  const app = express();
  const redis = new Redis(process.env.REDIS_URL);
  const RedisStore = connectRedis(session);

  const conn = await createConnection({
    type: "postgres",
    url: process.env.DATABASE_URL,
    logging: true,
    synchronize: __prod__ ? false : true,
    migrations: [path.join(__dirname, "./migrations/*")],
    entities: [User, Family, Wish],
  });
  if (__prod__) await conn.runMigrations();

  app.set("trust proxy", 1);

  app.use(
    cors({
      origin: process.env.CORS_ORIGIN,
      credentials: true,
    })
  );

  app.use(
    session({
      name: COOKIE_NAME,
      secret: process.env.SESSION_SECRET,
      store: new RedisStore({
        client: redis,
        disableTouch: true,
      }),
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, //10 years
        httpOnly: true,
        sameSite: "lax",
        secure: __prod__,
        domain: __prod__ ? "vanoce.guru" : "",
      },
    })
  );

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [UserResolver, FamilyResolver, WishResolver],
    }),
    plugins: [
      process.env.NODE_ENV === "production"
        ? ApolloServerPluginLandingPageDisabled()
        : ApolloServerPluginLandingPageGraphQLPlayground(),
    ],
    context: ({ req, res }) => ({
      req,
      res,
    }),
  });
  await apolloServer.start();

  apolloServer.applyMiddleware({
    app,
    cors: false,
  });

  app.get("", (_, res) => {
    res.redirect("/graphql");
  });

  app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });
};

main().catch(console.error);
