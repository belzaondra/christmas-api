import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialMigration1634364927555 implements MigrationInterface {
    name = 'InitialMigration1634364927555'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('USER', 'ADMIN')`);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "username" character varying(50) NOT NULL, "email" character varying(255) NOT NULL, "role" "user_role_enum" NOT NULL DEFAULT 'USER', "password" character varying NOT NULL, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "wish_state_enum" AS ENUM('NEW', 'PENDING', 'FINISHED')`);
        await queryRunner.query(`CREATE TABLE "wish" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying(100) NOT NULL, "link" character varying(300), "description" text NOT NULL, "state" "wish_state_enum" NOT NULL DEFAULT 'NEW', "userId" uuid, "giverId" uuid, CONSTRAINT "PK_e338d8f62014703650439326d3a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "family" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying(75) NOT NULL, CONSTRAINT "PK_ba386a5a59c3de8593cda4e5626" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "family_admins_user" ("familyId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_add0211bdacd567fd39b6d303b5" PRIMARY KEY ("familyId", "userId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f4817f990dec35bc06bfad3c59" ON "family_admins_user" ("familyId") `);
        await queryRunner.query(`CREATE INDEX "IDX_21799bbb908568c6919224c71f" ON "family_admins_user" ("userId") `);
        await queryRunner.query(`CREATE TABLE "family_members_user" ("familyId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_6f319888b635969165cbfdf87e0" PRIMARY KEY ("familyId", "userId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_74e685c6ed4e02657a46a29035" ON "family_members_user" ("familyId") `);
        await queryRunner.query(`CREATE INDEX "IDX_7b300b2682cd61c999b5cde8cb" ON "family_members_user" ("userId") `);
        await queryRunner.query(`CREATE TABLE "family_wishes_wish" ("familyId" uuid NOT NULL, "wishId" uuid NOT NULL, CONSTRAINT "PK_03615fde18d5046d580ab2b5d73" PRIMARY KEY ("familyId", "wishId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_94dee5701022b495c3d5d24e2a" ON "family_wishes_wish" ("familyId") `);
        await queryRunner.query(`CREATE INDEX "IDX_403a183e425b1d259d45aeacd8" ON "family_wishes_wish" ("wishId") `);
        await queryRunner.query(`ALTER TABLE "wish" ADD CONSTRAINT "FK_cc0ea2abf152cc78b535338c579" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "wish" ADD CONSTRAINT "FK_5dd2375281c14339c2154043bec" FOREIGN KEY ("giverId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "family_admins_user" ADD CONSTRAINT "FK_f4817f990dec35bc06bfad3c596" FOREIGN KEY ("familyId") REFERENCES "family"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "family_admins_user" ADD CONSTRAINT "FK_21799bbb908568c6919224c71f6" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "family_members_user" ADD CONSTRAINT "FK_74e685c6ed4e02657a46a290352" FOREIGN KEY ("familyId") REFERENCES "family"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "family_members_user" ADD CONSTRAINT "FK_7b300b2682cd61c999b5cde8cb0" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "family_wishes_wish" ADD CONSTRAINT "FK_94dee5701022b495c3d5d24e2af" FOREIGN KEY ("familyId") REFERENCES "family"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "family_wishes_wish" ADD CONSTRAINT "FK_403a183e425b1d259d45aeacd87" FOREIGN KEY ("wishId") REFERENCES "wish"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "family_wishes_wish" DROP CONSTRAINT "FK_403a183e425b1d259d45aeacd87"`);
        await queryRunner.query(`ALTER TABLE "family_wishes_wish" DROP CONSTRAINT "FK_94dee5701022b495c3d5d24e2af"`);
        await queryRunner.query(`ALTER TABLE "family_members_user" DROP CONSTRAINT "FK_7b300b2682cd61c999b5cde8cb0"`);
        await queryRunner.query(`ALTER TABLE "family_members_user" DROP CONSTRAINT "FK_74e685c6ed4e02657a46a290352"`);
        await queryRunner.query(`ALTER TABLE "family_admins_user" DROP CONSTRAINT "FK_21799bbb908568c6919224c71f6"`);
        await queryRunner.query(`ALTER TABLE "family_admins_user" DROP CONSTRAINT "FK_f4817f990dec35bc06bfad3c596"`);
        await queryRunner.query(`ALTER TABLE "wish" DROP CONSTRAINT "FK_5dd2375281c14339c2154043bec"`);
        await queryRunner.query(`ALTER TABLE "wish" DROP CONSTRAINT "FK_cc0ea2abf152cc78b535338c579"`);
        await queryRunner.query(`DROP INDEX "IDX_403a183e425b1d259d45aeacd8"`);
        await queryRunner.query(`DROP INDEX "IDX_94dee5701022b495c3d5d24e2a"`);
        await queryRunner.query(`DROP TABLE "family_wishes_wish"`);
        await queryRunner.query(`DROP INDEX "IDX_7b300b2682cd61c999b5cde8cb"`);
        await queryRunner.query(`DROP INDEX "IDX_74e685c6ed4e02657a46a29035"`);
        await queryRunner.query(`DROP TABLE "family_members_user"`);
        await queryRunner.query(`DROP INDEX "IDX_21799bbb908568c6919224c71f"`);
        await queryRunner.query(`DROP INDEX "IDX_f4817f990dec35bc06bfad3c59"`);
        await queryRunner.query(`DROP TABLE "family_admins_user"`);
        await queryRunner.query(`DROP TABLE "family"`);
        await queryRunner.query(`DROP TABLE "wish"`);
        await queryRunner.query(`DROP TYPE "wish_state_enum"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TYPE "user_role_enum"`);
    }

}
