import { MaxLength } from "class-validator";
import { InputType, Field } from "type-graphql";

@InputType()
export default class CreateWishInput {
  @Field()
  @MaxLength(255)
  name!: string;

  @Field()
  @MaxLength(255)
  description!: string;

  @Field()
  @MaxLength(300)
  link!: string;

  @Field()
  @MaxLength(300)
  familyId!: string;
}
