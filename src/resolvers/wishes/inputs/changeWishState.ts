import { MaxLength } from "class-validator";
import { InputType, Field } from "type-graphql";
import GiftState from "../../../types/giftState";

@InputType()
export default class ChangeWishStateInput {
  @Field()
  @MaxLength(255)
  id!: string;

  @Field()
  @MaxLength(255)
  state!: GiftState;
}
