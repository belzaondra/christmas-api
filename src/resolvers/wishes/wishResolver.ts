import { ApolloError } from "apollo-server-errors";
import { Arg, Ctx, Mutation, Resolver, UseMiddleware } from "type-graphql";
import { Family } from "../../entities/family";
import Wish from "../../entities/wish";
import { User } from "../../entities/user";
import { isAuth } from "../../middlewares/isAuth";
import { Context } from "../../types/Context";
import CreateWishInput from "./inputs/createWishInput";
import ChangeWishStateInput from "./inputs/changeWishState";
import GiftState from "../../types/giftState";
import UpdateWishInput from "./inputs/updateWishInput";

@Resolver(Wish)
export default class WishResolver {
  @Mutation(() => Wish)
  @UseMiddleware(isAuth)
  async createWish(
    @Ctx() { req }: Context,
    @Arg("model") model: CreateWishInput
  ): Promise<Wish> {
    const user = await User.findOne(req.session.userId);
    const family = await Family.findOne(model.familyId);

    if (!family) throw new ApolloError("rodina neexistuje");

    const wish = Wish.create({
      name: model.name,
      description: model.description,
      link: model.link,
      families: [family],
      user: user,
    });

    return wish.save();
  }

  @Mutation((_) => Boolean)
  @UseMiddleware(isAuth)
  async deleteWish(
    @Arg("id") id: string,
    @Arg("familyId") familyId: string,
    @Ctx() { req }: Context
  ): Promise<Boolean> {
    const user = await User.findOne(req.session.userId);
    const wish = await Wish.findOne(id, {
      relations: ["families"],
      where: { user },
    });

    if (wish) {
      wish.families = wish.families.filter((f) => f.id !== familyId);
      await wish.save();
      return true;
    }

    return false;
  }

  @Mutation((_) => Boolean)
  @UseMiddleware(isAuth)
  async changeWishState(
    @Arg("model") model: ChangeWishStateInput,
    @Ctx() { req }: Context
  ): Promise<Boolean> {
    const user = await User.findOne(req.session.userId);
    const wish = await Wish.findOne(model.id, {
      relations: ["families", "giver"],
    });

    if (wish && user) {
      if (model.state === "PENDING") {
        if (wish.giver) return false;

        wish.giver = user;
        wish.state = GiftState.Pending;
      } else if (model.state === "NEW") {
        if (!wish.giver || wish.giver.id !== user.id) return false;

        wish.giver = null;
        wish.state = GiftState.New;
      } else if (model.state === "FINISHED") {
        if (wish.giver && wish.giver.id !== user.id) return false;

        if (!wish.giver) wish.giver = user;
        wish.state = GiftState.Finished;
      }

      await wish.save();
      return true;
    }

    return false;
  }

  @Mutation((_) => Boolean)
  @UseMiddleware(isAuth)
  async updateWish(
    @Arg("model") model: UpdateWishInput,
    @Ctx() { req }: Context
  ): Promise<Boolean> {
    const user = await User.findOne(req.session.userId);
    const wish = await Wish.findOne(model.id, { where: { user } });

    if (wish && user) {
      wish.description = model.description;
      wish.link = model.link;
      wish.name = model.name;

      await wish.save();
      return true;
    }

    return false;
  }
}
