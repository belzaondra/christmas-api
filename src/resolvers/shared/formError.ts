import { Field, ObjectType } from "type-graphql";

@ObjectType()
export default class FormError {
  constructor(message?: string, field?: string) {
    if (message) this.message = message;
    if (field) this.field = field;
  }
  @Field({ nullable: true })
  field?: string;

  @Field()
  message!: string;
}
