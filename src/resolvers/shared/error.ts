import { Field, ObjectType } from "type-graphql";

@ObjectType()
export default class Error {
  constructor(message?: string) {
    if (message) this.message = message;
  }

  @Field()
  message!: string;
}
