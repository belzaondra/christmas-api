import { MaxLength } from "class-validator";
import { Field, InputType } from "type-graphql";

@InputType()
export default class RegistrationInput {
  @Field()
  @MaxLength(255)
  username!: string;

  @Field()
  @MaxLength(255)
  email!: string;

  @Field()
  @MaxLength(255)
  password!: string;
}
