import { MaxLength } from "class-validator";
import { Field, InputType } from "type-graphql";

@InputType()
export default class LoginInput {
  @Field()
  @MaxLength(255)
  username!: string;

  @Field()
  @MaxLength(255)
  password!: string;
}
