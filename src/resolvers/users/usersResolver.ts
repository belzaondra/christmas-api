import argon2 from "argon2";
import { Arg, Ctx, Mutation, Query, Resolver } from "type-graphql";
import { COOKIE_NAME } from "../../consants";
import { User } from "../../entities/user";
import { Context } from "../../types/Context";
import FormError from "../shared/formError";
import LoginInput from "./inputs/loginInput";
import RegistrationInput from "./inputs/registrationInput";
import UserFormResponse from "./outputs/userFormResponse";
import * as EmailValidator from "email-validator";

@Resolver(User)
export default class UserResolver {
  @Query((_) => User, { nullable: true })
  async me(@Ctx() { req }: Context): Promise<User | undefined> {
    let id = req.session.userId;
    if (!id) return undefined;
    return User.findOne(id);
  }

  @Mutation(() => UserFormResponse)
  // @Authorized()
  async register(
    @Ctx() { req }: Context,
    @Arg("RegistrationInput") newUserData: RegistrationInput
  ): Promise<UserFormResponse> {
    const response = new UserFormResponse();

    if (
      !newUserData.password ||
      newUserData.password.length < 2 ||
      newUserData.password.length > 255
    )
      response.addError(new FormError("neplatné heslo", "password"));

    if (
      !newUserData.username ||
      newUserData.username.length < 2 ||
      newUserData.username.length > 50
    )
      response.addError(
        new FormError("neplatné přihlašovací jméno", "username")
      );

    if (
      !newUserData.email ||
      newUserData.email.length < 2 ||
      newUserData.email.length > 255 ||
      !EmailValidator.validate(newUserData.email)
    )
      response.addError(new FormError("neplatný email", "email"));

    if (response.errors && response.errors.length) return response;

    const dbUser = await User.findOne({
      where: [{ username: newUserData.username }, { email: newUserData.email }],
    });

    if (dbUser) {
      response.addError(
        new FormError("email nebo uživatelské jméno je již použito")
      );
      return response;
    }

    const user = new User();
    user.username = newUserData.username;
    user.email = newUserData.email;
    user.password = await argon2.hash(newUserData.password);
    await user.save();

    req.session.userId = user.id;
    response.user = user;
    return response;
  }

  @Mutation(() => UserFormResponse)
  // @Authorized()
  async login(
    @Arg("loginInput") loginData: LoginInput,
    @Ctx() { req }: Context
  ): Promise<UserFormResponse> {
    const response = new UserFormResponse();

    const user = await User.findOne({
      where: { username: loginData.username },
    });

    if (!user || !(await argon2.verify(user.password, loginData.password))) {
      response.addError(
        new FormError("neplatné přihlašovací jméno nebo heslo")
      );
      return response;
    }
    req.session.userId = user.id;

    response.user = user;
    return response;
  }

  @Mutation(() => Boolean)
  logout(@Ctx() { req, res }: Context): Promise<Boolean> {
    return new Promise((resolve) => {
      req.session.destroy((err) => {
        res.clearCookie(COOKIE_NAME);
        if (err) {
          resolve(false);
          return;
        } else resolve(true);
      });
    });
  }
}
