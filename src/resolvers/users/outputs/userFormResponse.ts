import { Field, ObjectType } from "type-graphql";
import { User } from "../../../entities/user";
import FormError from "../../shared/formError";

@ObjectType()
export default class UserFormResponse {
  constructor(user?: User) {
    if (user) this.user = user;
  }

  addError(error: FormError) {
    if (!this.errors) this.errors = [];
    this.errors.push(error);
  }
  @Field({ nullable: true })
  user!: User;

  @Field((_) => [FormError], { nullable: true })
  errors?: FormError[];
}
