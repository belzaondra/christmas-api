import { ObjectType, Field } from "type-graphql";
import { Family } from "../../../entities/family";
import FormError from "../../shared/formError";

@ObjectType()
export default class CreateFamilyResponse {
  constructor(family?: Family) {
    if (family) this.family = family;
  }

  addError(error: FormError) {
    if (!this.errors) this.errors = [];
    this.errors.push(error);
  }
  @Field({ nullable: true })
  family!: Family;

  @Field((_) => [FormError], { nullable: true })
  errors?: FormError[];
}
