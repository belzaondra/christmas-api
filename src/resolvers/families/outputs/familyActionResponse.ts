import { Field, ObjectType } from "type-graphql";
import Error from "../../shared/error";
@ObjectType()
export default class FamilyActionResponse {
  constructor(result?: Boolean) {
    if (result) this.result = result;
  }

  addError(error: Error) {
    if (!this.errors) this.errors = [];
    this.errors.push(error);
  }
  @Field()
  result: Boolean = false;

  @Field((_) => [Error], { nullable: true })
  errors?: Error[];
}
