import {
  Arg,
  Ctx,
  Mutation,
  Query,
  Resolver,
  UseMiddleware,
} from "type-graphql";
import { Family } from "../../entities/family";
import { User } from "../../entities/user";
import { isAdmin } from "../../middlewares/isAdmin";
import { isAuth } from "../../middlewares/isAuth";
import { Context } from "../../types/Context";
import Error from "../shared/error";
import FormError from "../shared/formError";
import CreateFamilyResponse from "./outputs/createFamilyResponse";
import FamilyActionResponse from "./outputs/familyActionResponse";

@Resolver(Family)
export default class FamilyResolver {
  @Query((_) => [Family])
  @UseMiddleware(isAdmin)
  families(@Ctx() {}: Context) {
    return Family.find({ relations: ["admins", "members"] });
  }

  @Query((_) => [Family])
  @UseMiddleware(isAuth)
  async myFamilies(@Ctx() { req }: Context) {
    const userId = req.session.userId;

    const families = await Family.find({
      relations: ["admins", "members", "wishes"],
    });

    return families.filter((f) => f.members.some((m) => m.id === userId));
  }

  @Query((_) => Family)
  @UseMiddleware(isAuth)
  async family(
    @Arg("id") id: string,
    @Ctx() {}: Context
  ): Promise<Family | undefined> {
    return Family.findOne({
      where: { id },
      relations: ["admins", "members", "wishes", "wishes.user", "wishes.giver"],
    });
  }

  @Mutation((_) => CreateFamilyResponse)
  @UseMiddleware(isAuth)
  async createFamily(
    @Arg("name") name: string,
    @Ctx() { req }: Context
  ): Promise<CreateFamilyResponse> {
    const response = new CreateFamilyResponse();

    const userId = req.session.userId;
    const user = await User.findOne(userId);

    if (!user) response.addError(new FormError("uživatel nenalezen"));

    if (!name || name.length < 2 || name.length > 75)
      response.addError(new FormError("neplatné jméno rodiny", "name"));

    if (response.errors && response.errors.length) return response;

    const family = await Family.create({
      name,
      admins: [user as User],
      members: [user as User],
    }).save();
    response.family = family;
    return response;
  }

  @Mutation((_) => FamilyActionResponse)
  @UseMiddleware(isAuth)
  async deleteFamily(
    @Arg("id") id: string,
    @Ctx() { req }: Context
  ): Promise<FamilyActionResponse> {
    const response = new FamilyActionResponse();
    const userId = req.session.userId;

    const family = await Family.findOne({ id }, { relations: ["admins"] });
    const user = await User.findOne(userId);

    if (!user) response.addError(new Error("uživatel nenalezen"));
    if (!family) response.addError(new Error("rodina nenalezen"));
    else if (!family.admins.some((admin) => admin.id === userId))
      response.addError(new Error("nedostatečná oprávnění"));

    if (response.errors && response.errors.length) return response;

    const result = await Family.delete(id);
    response.result = result.affected ? true : false;
    return response;
  }

  @Mutation((_) => FamilyActionResponse)
  @UseMiddleware(isAuth)
  async joinFamily(
    @Arg("id") id: string,
    @Ctx() { req }: Context
  ): Promise<FamilyActionResponse> {
    const response = new FamilyActionResponse();

    let family = await Family.findOne({ id }, { relations: ["members"] });
    let user = await User.findOne({ id: req.session.userId });

    if (!family) response.addError(new Error("rodina nenalezena"));
    else if (family.members.some((member) => member.id === req.session.userId))
      response.addError(new Error("v této rodině již jste"));
    if (!user) response.addError(new Error("uživatel nenalezen"));

    if (response.errors && response.errors.length) return response;

    family = family as Family;
    !family.members.push(user as User);
    await family.save();
    response.result = true;
    return response;
  }

  @Mutation((_) => FamilyActionResponse)
  async leaveFamily(
    @Arg("id") id: string,
    @Ctx() { req }: Context
  ): Promise<FamilyActionResponse> {
    const response = new FamilyActionResponse();
    const userId = req.session.userId;

    let family = await Family.findOne(
      { id },
      { relations: ["members", "admins"] }
    );

    let user = await User.findOne(userId, {
      relations: ["wishes", "wishes.families"],
    });

    if (!family) response.addError(new Error("rodina nenalezena"));
    else if (!family.members.some((member) => member.id === req.session.userId))
      response.addError(new Error("v této rodině již nejste"));

    if (response.errors && response.errors.length) return response;

    family = family as Family;

    family.members = family.members.filter((m) => m.id !== userId);
    family.admins = family.admins.filter((m) => m.id !== userId);

    if (user?.wishes)
      user.wishes = user.wishes.map((g) => {
        g.families = g.families.filter((f) => f.id !== id);
        return g;
      });

    await family.save();
    await user?.save();
    response.result = true;
    return response;
  }
}
