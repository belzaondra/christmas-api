declare namespace NodeJS {
  interface ProcessEnv {
    PORT: string;
    SESSION_SECRET: string;
    DATABASE_URL: string;
    CORS_ORIGIN: string;
    REDIS_URL: string;
  }
}