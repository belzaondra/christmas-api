import { ApolloError } from "apollo-server-errors";
import { MiddlewareFn } from "type-graphql";
import { Context } from "../types/Context";

export const isAuth: MiddlewareFn<Context> = async ({ context }, next) => {
  if (!context.req.session.userId)
    throw new ApolloError("uživatel není přihlášen", "USER_NOT_LOGGED_IN");

  return next();
};
