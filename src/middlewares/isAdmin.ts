import { ApolloError } from "apollo-server";
import { MiddlewareFn } from "type-graphql";
import { User } from "../entities/user";
import { Context } from "../types/Context";
import UserRole from "../types/userRole";

export const isAdmin: MiddlewareFn<Context> = async ({ context }, next) => {
  if (!context.req.session.userId)
    throw new ApolloError("uživatel není přihlášen", "USER_NOT_LOGGED_IN");

  const user = await User.findOne(context.req.session.userId);
  if (!user) throw new ApolloError("uživatel neexistuje", "USER_NOT_FOUND");
  if (user.role !== UserRole.Admin)
    throw new ApolloError("nedostatečná oprávnění", "FORBIDDEN");

  return next();
};
